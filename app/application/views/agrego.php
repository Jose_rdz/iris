<?php
if (!isset($_SESSION['nombre'])) {

    header('Location: '.base_url(''));
}else{
    if (isset($_POST['number'])) {
        $number = $_POST['number'];
        $query = 'INSERT INTO validaciones(id, validados) VALUES ('.$_SESSION['id'].','.$number.')';
        $resultados = $this->db->query($query);
        $_SESSION['validacion'] = 1;
        // header('Location: '.base_url('AgregarUsuarios'));
    }
    ?>
    <!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iris</title>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Estos son nuestros estilos -->
    <link rel="stylesheet" href="./css/index.css">
    <!-- Nuestras fuentes -->
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
</head>

<body>

<nav>
    <?php echo $navbar; ?> 
</nav>


    <?php 
if (isset($_SESSION['validacion'])) {
    $resultado ='  <p style="text-align:center;">Tu codigo es: <br> '.$_SESSION['clave'].'</p>';
  echo $resultado;
}else{
    echo $form; 
}

  ?>

    <?php echo $footer; ?>
    

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <!--Aqui van nuestros js  -->
    <script src="./js/index.js"></script>
    <script>M.AutoInit();</script>
    
</body>

</html>
<?php
}
?>