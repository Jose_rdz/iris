
<div ng-app="mainModule" ng-controller="mainController" class="container">
    <div id="login-page" class="row">
        <div class="col s12 z-depth-6 card-panel">

            <form class="login-form" action="<?php echo base_url('')?>" method="POST">


                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">mail_outline</i>
                        <input class="validate" name="email" id="email" type="email">
                        <label for="email" data-error="wrong" data-success="right">Correo</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">lock_outline</i>
                        <input id="password" name="pass" type="password">
                        <label for="password">Contraseña</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <button type="submit" class="btn waves-effect waves-light col s12 blue darken-4">Iniciar sesion</button>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <a href="<?php echo base_url('Registro')?>" class="blue-text text-darken-4">Registrarme</a>
                    </div>        
                </div>

            </form>

        </div>
    </div> 
</div>
