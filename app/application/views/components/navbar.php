<?php
 if (!isset($_SESSION['nombre'])) {
     ?>
     <div class="nav-wrapper blue darken-4">
        <div class="container">
            <a href="<?php echo base_url('')?>" class="brand-logo">Iris</a>
            <ul class="right hide-on-med-and-down">
                <li><a href="<?php echo base_url('')?>">Iniciar Sesion</a></li>
                <li><a href="<?php echo base_url('registro')?>">Registrarme</a></li>
            </ul>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger">
                <i class="material-icons">menu</i>
            </a>
    
        </div>
        </div>
        <ul class="sidenav" id="mobile-demo">
            <li><a href="<?php echo base_url('')?>">Iniciar Sesion</a></li>
            <li><a href="<?php echo base_url('registro')?>">Registrarme</a></li>
        </ul>
<?php
 }else{
    ?>
         <div class="nav-wrapper blue darken-4">
        <div class="container">
            <a href="<?php echo base_url('')?>" class="brand-logo">Iris</a>
            <ul class="right hide-on-med-and-down">
                <li><a href="<?php echo base_url('AgregarUsuarios')?>">Añadir Usuarios</a></li>
                <li><a href="<?php echo base_url('CerrarSession');?>">Cerrar Session</a></li>
            </ul>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger">
                <i class="material-icons">menu</i>
            </a>
    
        </div>
        </div>
        <ul class="sidenav" id="mobile-demo">
            <li><a href="<?php echo base_url('AgregarUsuarios')?>">Añadir Usuarios</a></li>
            <li><a href="<?php echo base_url('CerrarSession');?>">Cerrar Session</a></li>
        </ul>
    
    
    <?php
   
 }
?>