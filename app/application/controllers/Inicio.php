<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		$dato['navbar'] = $this->load->view('./components/navbar', '' , TRUE);
		$dato['mapa'] = $this->load->view('./components/mapa', '' , TRUE);
		$dato['form'] = $this->load->view('./components/iniciaSesion', '', TRUE);
		$dato['footer'] = $this->load->view('./components/footer', '', TRUE);
		$this->load->view('./inicio', $dato);
	}
}
