// Estilos del mapa.
const stylesMap = [
  {
    "featureType": "all",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#202c3e"
      }
    ]
  },
  {
    "featureType": "all",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "gamma": 0.01
      },
      {
        "lightness": 20
      },
      {
        "weight": "1.39"
      },
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "all",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "weight": "0.96"
      },
      {
        "saturation": "9"
      },
      {
        "visibility": "on"
      },
      {
        "color": "#000000"
      }
    ]
  },
  {
    "featureType": "all",
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "landscape",
    "elementType": "geometry",
    "stylers": [
      {
        "lightness": 30
      },
      {
        "saturation": "9"
      },
      {
        "color": "#29446b"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "saturation": 20
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "lightness": 20
      },
      {
        "saturation": -20
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "lightness": 10
      },
      {
        "saturation": -30
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#193a55"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "saturation": 25
      },
      {
        "lightness": 25
      },
      {
        "weight": "0.01"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "all",
    "stylers": [
      {
        "lightness": -20
      }
    ]
  }
]

/**
* **** **** **** Variales GLOBALES **** **** ****
*/

let transito = true;
let modoGuardarPuntosDeInteres = false;

// -------------------------------------------------- \\
// -------------------------------------------------- \\

/**
* **** **** **** Creacion del MAPA **** **** ****
*/
function iniciarMapa(lat, lng) {
  let posicion = { lat, lng };
  let map = new google.maps.Map(document.getElementById("map"), {
    zoom: 17,
    center: posicion,
    zoomControl: true,
    mapTypeControl: false,
    scaleControl: true,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: false,
    styles: stylesMap
  });

  // añadiendo el marker personalizado
  const image = 'https://i.imgur.com/SmGN1Bi.png';
  const marker = new google.maps.Marker({
    position: { lat, lng },
    map: map,
    icon: image
  });
  // añadiendo el popup 
  const infowindow = new google.maps.InfoWindow({
    content: 'Tú estas aquí.'
  });
  // añadiendo el evento para el popup
  marker.addListener('click', function () {
    infowindow.open(map, marker);
  });

  if (transito) {
    const trafficLayer = new google.maps.TrafficLayer();
    trafficLayer.setMap(map);
  }

  map.addListener('click', anadirPuntosDeInteres);

  let coords;

  function anadirPuntosDeInteres(event) {
    const { lat } = event.latLng;
    const { lng } = event.latLng;
    const latitud = lat();
    const longitud = lng();
    coords = {
      lat: latitud,
      lng: longitud
    }

    if (modoGuardarPuntosDeInteres) {
      ubicacion(posicion, coords, map)
    }
  }

}

function ubicacion(origen, destino, map) {
  const objConfig = {
    origin: origen,
    destination: destino,
    travelMode: google.maps.TravelMode.WALKING
  }
  const ds = new google.maps.DirectionsService();
  const dr = new google.maps.DirectionsRenderer({ map });

  ds.route(objConfig, (resultados, estado) => {
    if (estado === 'OK') {
      dr.setDirections(resultados)
    } else {
      console.log('Error')
    }
  });
}



// -------------------------------------------------- \\
// -------------------------------------------------- \\
/**
  **** **** **** Ubicacion del usuario **** **** ****
*/
function geoFindMe() {
  function success(position) {
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;

    let coords = {
      lat: latitude,
      lng: longitude
    }

    iniciarMapa(coords.lat, coords.lng);
  };

  function error() {
    console.log('Unable to retrieve your location');
  };

  navigator.geolocation.getCurrentPosition(success, error);
}

function activeMap() {
  const permiso = confirm('Deseas compartir tu ubicacion?');

  if (permiso) {
    geoFindMe();
  } else {
    alert('ok. =)')
  }
}



// -------------------------------------------------- \\
// -------------------------------------------------- \\
/**
  **** **** **** Manipulacion con el menu. **** **** ****
*/

const directions_bus = document.getElementById('directions_bus');
directions_bus.addEventListener('click', () => {

  console.log('directions_bus')

});

const person = document.getElementById('person');
person.addEventListener('click', () => {
  modoGuardarPuntosDeInteres = !modoGuardarPuntosDeInteres;
});

const build = document.getElementById('build');
build.addEventListener('click', () => {
  transito = !transito;
  geoFindMe();
});
